document.addEventListener("DOMContentLoaded", function(event) {


    var connexion = new MovieDB();


    if(location.pathname.search("fiche-film.html") > 0){

        var params = ( new URL(document.location) ).searchParams;

        console.log(params.get("id") );

        connexion.requeteInfoFilm(params.get("id") );

    }
    else{
        connexion.requeteFilmPopulaire();
    }

//Un commentaire
    console.log("Ça fonctionne");

    /** Scroll to top button implementation in vanilla JavaScript (ES6 - ECMAScript 6) **/

    var intervalId = 0; // Needed to cancel the scrolling when we're at the top of the page
    var $scrollButton = document.querySelector('.scroll'); // Reference to our scroll button

    function scrollStep() {
        // Check if we're at the top already. If so, stop scrolling by clearing the interval
        if (window.pageYOffset === 0) {
            clearInterval(intervalId);
        }
        window.scroll(0, window.pageYOffset - 50);
    }

    function scrollToTop() {
        // Call the function scrollStep() every 16.66 millisecons
        intervalId = setInterval(scrollStep, 16.66);
    }
    // When the DOM is loaded, this click handler is added to our scroll button
    $scrollButton.addEventListener('click', scrollToTop);






    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
//});

});










class MovieDB{

    constructor(){
        console.log("Parfait 2");

        this.APIKey = "eda01ad95b124c2be1b5f4308d87648f";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;

        this.totalActeur = 3;
    }

    requeteFilmPopulaire(){


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmPopulaire(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse( target.responseText ).results;

            this.afficheFilmPopulaire(data);

            // this.afficheFilmPopulaire2(data);



        }

    }
    // afficheFilmPopulaire2(data) {
    //
    //     console.log(data);
    //
    //     for(var i=0; i<this.totalFilm; i++){
    //
    //
    //         var unArticle = document.querySelector(".template2>.film").cloneNode(true);
    //
    //
    //         unArticle.querySelector("h1").innerText = data[i].title;
    //
    //         unArticle.querySelector("img").setAttribute("src", this.imgPath + "w185" + data[i].poster_path);
    //         unArticle.querySelector("img").setAttribute("alt", data[i].title);
    //
    //
    //
    //
    //         document.querySelector(".swiper-slide").appendChild(unArticle);
    //
    //     }
    //
    // }




    afficheFilmPopulaire(data) {

        console.log(data);

        for(var i=0; i<this.totalFilm; i++){


            var unArticle = document.querySelector(".template>.film").cloneNode(true);


            unArticle.querySelector("h1").innerText = data[i].title;

            if(data[i].overview === ""){
                unArticle.querySelector(".description").innerText = "Description présentement non disponible"
            }
            else{
                unArticle.querySelector(".description").innerText = data[i].overview;
            }

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);
            unArticle.querySelector("img").setAttribute("alt", data[i].title);

            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);


            document.querySelector(".liste-films").appendChild(unArticle);

            document.querySelector(".rating").innerText = data.vote_average;

            document.querySelector(".release").innerText = data.release_date;


        }

    }








    // Requete pour un film en particulier

    requeteInfoFilm(movieId){

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfoFilm.bind(this) );

        // xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();

    }


    retourInfoFilm(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse( target.responseText );

            this.afficheInfoFilm(data);


        }

    }

    afficheInfoFilm(data){

        console.log(data);

        document.querySelector("h1").innerText = data.title;

        if(data.overview === ""){
            document.querySelector(".description2").innerText = "Description présentement non disponible"
        }
        else{
            document.querySelector(".description2").innerText = data.overview;
        }



        document.querySelector("img.affiche").setAttribute("src", this.imgPath + "w500" + data.poster_path);

        this.requeteActeur( data.id );





    }


    requeteActeur(movieId){


        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourActeur.bind(this));

        // xhr.open("GET", "https://api.themoviedb.org/3/credit/%7Bcredit_id%7D?api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId +"/credits?api_key=" + this.APIKey);

        xhr.send();
    }


    retourActeur(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse( target.responseText).cast;

            this.afficheActeur(data);



        }

    }



    afficheActeur(data){
        console.log(data);

        for(var i=0; i<this.totalActeur; i++){

            var unActeur = document.querySelector(".template>.acteur").cloneNode(true);


            unActeur.querySelector("h4").innerText = data[i].name;


            unActeur.querySelector("img").setAttribute("src", this.imgPath + "w185" + data[i].profile_path);
            unActeur.querySelector("img").setAttribute("alt", data[i].name);

            document.querySelector(".liste-acteurs").appendChild(unActeur);

        }



    }



}




//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24oZXZlbnQpIHtcclxuXHJcblxyXG4gICAgdmFyIGNvbm5leGlvbiA9IG5ldyBNb3ZpZURCKCk7XHJcblxyXG5cclxuICAgIGlmKGxvY2F0aW9uLnBhdGhuYW1lLnNlYXJjaChcImZpY2hlLWZpbG0uaHRtbFwiKSA+IDApe1xyXG5cclxuICAgICAgICB2YXIgcGFyYW1zID0gKCBuZXcgVVJMKGRvY3VtZW50LmxvY2F0aW9uKSApLnNlYXJjaFBhcmFtcztcclxuXHJcbiAgICAgICAgY29uc29sZS5sb2cocGFyYW1zLmdldChcImlkXCIpICk7XHJcblxyXG4gICAgICAgIGNvbm5leGlvbi5yZXF1ZXRlSW5mb0ZpbG0ocGFyYW1zLmdldChcImlkXCIpICk7XHJcblxyXG4gICAgfVxyXG4gICAgZWxzZXtcclxuICAgICAgICBjb25uZXhpb24ucmVxdWV0ZUZpbG1Qb3B1bGFpcmUoKTtcclxuICAgIH1cclxuXHJcbi8vVW4gY29tbWVudGFpcmVcclxuICAgIGNvbnNvbGUubG9nKFwiw4dhIGZvbmN0aW9ubmVcIik7XHJcblxyXG4gICAgLyoqIFNjcm9sbCB0byB0b3AgYnV0dG9uIGltcGxlbWVudGF0aW9uIGluIHZhbmlsbGEgSmF2YVNjcmlwdCAoRVM2IC0gRUNNQVNjcmlwdCA2KSAqKi9cclxuXHJcbiAgICB2YXIgaW50ZXJ2YWxJZCA9IDA7IC8vIE5lZWRlZCB0byBjYW5jZWwgdGhlIHNjcm9sbGluZyB3aGVuIHdlJ3JlIGF0IHRoZSB0b3Agb2YgdGhlIHBhZ2VcclxuICAgIHZhciAkc2Nyb2xsQnV0dG9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNjcm9sbCcpOyAvLyBSZWZlcmVuY2UgdG8gb3VyIHNjcm9sbCBidXR0b25cclxuXHJcbiAgICBmdW5jdGlvbiBzY3JvbGxTdGVwKCkge1xyXG4gICAgICAgIC8vIENoZWNrIGlmIHdlJ3JlIGF0IHRoZSB0b3AgYWxyZWFkeS4gSWYgc28sIHN0b3Agc2Nyb2xsaW5nIGJ5IGNsZWFyaW5nIHRoZSBpbnRlcnZhbFxyXG4gICAgICAgIGlmICh3aW5kb3cucGFnZVlPZmZzZXQgPT09IDApIHtcclxuICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbElkKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgd2luZG93LnNjcm9sbCgwLCB3aW5kb3cucGFnZVlPZmZzZXQgLSA1MCk7XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gc2Nyb2xsVG9Ub3AoKSB7XHJcbiAgICAgICAgLy8gQ2FsbCB0aGUgZnVuY3Rpb24gc2Nyb2xsU3RlcCgpIGV2ZXJ5IDE2LjY2IG1pbGxpc2Vjb25zXHJcbiAgICAgICAgaW50ZXJ2YWxJZCA9IHNldEludGVydmFsKHNjcm9sbFN0ZXAsIDE2LjY2KTtcclxuICAgIH1cclxuICAgIC8vIFdoZW4gdGhlIERPTSBpcyBsb2FkZWQsIHRoaXMgY2xpY2sgaGFuZGxlciBpcyBhZGRlZCB0byBvdXIgc2Nyb2xsIGJ1dHRvblxyXG4gICAgJHNjcm9sbEJ1dHRvbi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHNjcm9sbFRvVG9wKTtcclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuICAgIHZhciBzd2lwZXIgPSBuZXcgU3dpcGVyKCcuc3dpcGVyLWNvbnRhaW5lcicsIHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAzLFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMzAsXHJcbiAgICAgICAgcGFnaW5hdGlvbjoge1xyXG4gICAgICAgICAgICBlbDogJy5zd2lwZXItcGFnaW5hdGlvbicsXHJcbiAgICAgICAgICAgIGNsaWNrYWJsZTogdHJ1ZSxcclxuICAgICAgICB9LFxyXG4gICAgfSk7XHJcbi8vfSk7XHJcblxyXG59KTtcclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbmNsYXNzIE1vdmllREJ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIlBhcmZhaXQgMlwiKTtcclxuXHJcbiAgICAgICAgdGhpcy5BUElLZXkgPSBcImVkYTAxYWQ5NWIxMjRjMmJlMWI1ZjQzMDhkODc2NDhmXCI7XHJcblxyXG4gICAgICAgIHRoaXMubGFuZyA9IFwiZnItQ0FcIjtcclxuXHJcbiAgICAgICAgdGhpcy5iYXNlVVJMID0gXCJodHRwczovL2FwaS50aGVtb3ZpZWRiLm9yZy8zL1wiO1xyXG5cclxuICAgICAgICB0aGlzLmltZ1BhdGggPSBcImh0dHBzOi8vaW1hZ2UudG1kYi5vcmcvdC9wL1wiO1xyXG5cclxuICAgICAgICB0aGlzLmxhcmdldXJBZmZpY2hlID0gW1wiOTJcIiwgXCIxNTRcIiwgXCIxODVcIiwgXCIzNDJcIiwgXCI1MDBcIiwgXCI3ODBcIl07XHJcblxyXG4gICAgICAgIHRoaXMubGFyZ2V1clRldGVBZmZpY2hlID0gW1wiNDVcIiwgXCIxODVcIl07XHJcblxyXG4gICAgICAgIHRoaXMudG90YWxGaWxtID0gNjtcclxuXHJcbiAgICAgICAgdGhpcy50b3RhbEFjdGV1ciA9IDM7XHJcbiAgICB9XHJcblxyXG4gICAgcmVxdWV0ZUZpbG1Qb3B1bGFpcmUoKXtcclxuXHJcblxyXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuXHJcblxyXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJldG91ckZpbG1Qb3B1bGFpcmUuYmluZCh0aGlzKSk7XHJcblxyXG4gICAgICAgIC8veGhyLm9wZW4oXCJHRVRcIiwgXCJodHRwczovL2FwaS50aGVtb3ZpZWRiLm9yZy8zL21vdmllL3BvcHVsYXI/cGFnZT0xJmxhbmd1YWdlPWVuLVVTJmFwaV9rZXk9JTNDJTNDYXBpX2tleSUzRSUzRVwiKTtcclxuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwgKyBcIm1vdmllL3BvcHVsYXI/cGFnZT0xJmxhbmd1YWdlPVwiICsgdGhpcy5sYW5nICsgXCImYXBpX2tleT1cIiArIHRoaXMuQVBJS2V5KTtcclxuXHJcbiAgICAgICAgeGhyLnNlbmQoKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgcmV0b3VyRmlsbVBvcHVsYWlyZShlKXtcclxuXHJcbiAgICAgICAgdmFyIHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcclxuXHJcbiAgICAgICAgdmFyIGRhdGE7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcclxuXHJcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKCB0YXJnZXQucmVzcG9uc2VUZXh0ICkucmVzdWx0cztcclxuXHJcbiAgICAgICAgICAgIHRoaXMuYWZmaWNoZUZpbG1Qb3B1bGFpcmUoZGF0YSk7XHJcblxyXG4gICAgICAgICAgICAvLyB0aGlzLmFmZmljaGVGaWxtUG9wdWxhaXJlMihkYXRhKTtcclxuXHJcblxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG4gICAgLy8gYWZmaWNoZUZpbG1Qb3B1bGFpcmUyKGRhdGEpIHtcclxuICAgIC8vXHJcbiAgICAvLyAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAvL1xyXG4gICAgLy8gICAgIGZvcih2YXIgaT0wOyBpPHRoaXMudG90YWxGaWxtOyBpKyspe1xyXG4gICAgLy9cclxuICAgIC8vXHJcbiAgICAvLyAgICAgICAgIHZhciB1bkFydGljbGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnRlbXBsYXRlMj4uZmlsbVwiKS5jbG9uZU5vZGUodHJ1ZSk7XHJcbiAgICAvL1xyXG4gICAgLy9cclxuICAgIC8vICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJoMVwiKS5pbm5lclRleHQgPSBkYXRhW2ldLnRpdGxlO1xyXG4gICAgLy9cclxuICAgIC8vICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwidzE4NVwiICsgZGF0YVtpXS5wb3N0ZXJfcGF0aCk7XHJcbiAgICAvLyAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiaW1nXCIpLnNldEF0dHJpYnV0ZShcImFsdFwiLCBkYXRhW2ldLnRpdGxlKTtcclxuICAgIC8vXHJcbiAgICAvL1xyXG4gICAgLy9cclxuICAgIC8vXHJcbiAgICAvLyAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuc3dpcGVyLXNsaWRlXCIpLmFwcGVuZENoaWxkKHVuQXJ0aWNsZSk7XHJcbiAgICAvL1xyXG4gICAgLy8gICAgIH1cclxuICAgIC8vXHJcbiAgICAvLyB9XHJcblxyXG5cclxuXHJcblxyXG4gICAgYWZmaWNoZUZpbG1Qb3B1bGFpcmUoZGF0YSkge1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuXHJcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8dGhpcy50b3RhbEZpbG07IGkrKyl7XHJcblxyXG5cclxuICAgICAgICAgICAgdmFyIHVuQXJ0aWNsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudGVtcGxhdGU+LmZpbG1cIikuY2xvbmVOb2RlKHRydWUpO1xyXG5cclxuXHJcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiaDFcIikuaW5uZXJUZXh0ID0gZGF0YVtpXS50aXRsZTtcclxuXHJcbiAgICAgICAgICAgIGlmKGRhdGFbaV0ub3ZlcnZpZXcgPT09IFwiXCIpe1xyXG4gICAgICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCIuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gXCJEZXNjcmlwdGlvbiBwcsOpc2VudGVtZW50IG5vbiBkaXNwb25pYmxlXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCIuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gZGF0YVtpXS5vdmVydmlldztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwidzUwMFwiICsgZGF0YVtpXS5wb3N0ZXJfcGF0aCk7XHJcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiaW1nXCIpLnNldEF0dHJpYnV0ZShcImFsdFwiLCBkYXRhW2ldLnRpdGxlKTtcclxuXHJcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiYVwiKS5zZXRBdHRyaWJ1dGUoXCJocmVmXCIsIFwiZmljaGUtZmlsbS5odG1sP2lkPVwiICsgZGF0YVtpXS5pZCk7XHJcblxyXG5cclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5saXN0ZS1maWxtc1wiKS5hcHBlbmRDaGlsZCh1bkFydGljbGUpO1xyXG5cclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5yYXRpbmdcIikuaW5uZXJUZXh0ID0gZGF0YS52b3RlX2F2ZXJhZ2U7XHJcblxyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnJlbGVhc2VcIikuaW5uZXJUZXh0ID0gZGF0YS5yZWxlYXNlX2RhdGU7XHJcblxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbiAgICAvLyBSZXF1ZXRlIHBvdXIgdW4gZmlsbSBlbiBwYXJ0aWN1bGllclxyXG5cclxuICAgIHJlcXVldGVJbmZvRmlsbShtb3ZpZUlkKXtcclxuXHJcbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG5cclxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJJbmZvRmlsbS5iaW5kKHRoaXMpICk7XHJcblxyXG4gICAgICAgIC8vIHhoci5vcGVuKFwiR0VUXCIsIFwiaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9tb3ZpZS8lN0Jtb3ZpZV9pZCU3RD9sYW5ndWFnZT1lbi1VUyZhcGlfa2V5PSUzQyUzQ2FwaV9rZXklM0UlM0VcIik7XHJcbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMICsgXCJtb3ZpZS9cIiArIG1vdmllSWQgKyBcIj9sYW5ndWFnZT1cIiArIHRoaXMubGFuZyArIFwiJmFwaV9rZXk9XCIgKyB0aGlzLkFQSUtleSk7XHJcblxyXG4gICAgICAgIHhoci5zZW5kKCk7XHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAgICByZXRvdXJJbmZvRmlsbShlKXtcclxuXHJcbiAgICAgICAgdmFyIHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcclxuXHJcbiAgICAgICAgdmFyIGRhdGE7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcclxuXHJcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKCB0YXJnZXQucmVzcG9uc2VUZXh0ICk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmFmZmljaGVJbmZvRmlsbShkYXRhKTtcclxuXHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgYWZmaWNoZUluZm9GaWxtKGRhdGEpe1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuXHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImgxXCIpLmlubmVyVGV4dCA9IGRhdGEudGl0bGU7XHJcblxyXG4gICAgICAgIGlmKGRhdGEub3ZlcnZpZXcgPT09IFwiXCIpe1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmRlc2NyaXB0aW9uMlwiKS5pbm5lclRleHQgPSBcIkRlc2NyaXB0aW9uIHByw6lzZW50ZW1lbnQgbm9uIGRpc3BvbmlibGVcIlxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmRlc2NyaXB0aW9uMlwiKS5pbm5lclRleHQgPSBkYXRhLm92ZXJ2aWV3O1xyXG4gICAgICAgIH1cclxuXHJcblxyXG5cclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiaW1nLmFmZmljaGVcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwidzUwMFwiICsgZGF0YS5wb3N0ZXJfcGF0aCk7XHJcblxyXG4gICAgICAgIHRoaXMucmVxdWV0ZUFjdGV1ciggZGF0YS5pZCApO1xyXG5cclxuXHJcblxyXG5cclxuXHJcbiAgICB9XHJcblxyXG5cclxuICAgIHJlcXVldGVBY3RldXIobW92aWVJZCl7XHJcblxyXG5cclxuICAgICAgICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcblxyXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJldG91ckFjdGV1ci5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAgICAgLy8geGhyLm9wZW4oXCJHRVRcIiwgXCJodHRwczovL2FwaS50aGVtb3ZpZWRiLm9yZy8zL2NyZWRpdC8lN0JjcmVkaXRfaWQlN0Q/YXBpX2tleT0lM0MlM0NhcGlfa2V5JTNFJTNFXCIpO1xyXG4gICAgICAgIHhoci5vcGVuKFwiR0VUXCIsIHRoaXMuYmFzZVVSTCArIFwibW92aWUvXCIgKyBtb3ZpZUlkICtcIi9jcmVkaXRzP2FwaV9rZXk9XCIgKyB0aGlzLkFQSUtleSk7XHJcblxyXG4gICAgICAgIHhoci5zZW5kKCk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIHJldG91ckFjdGV1cihlKXtcclxuXHJcbiAgICAgICAgdmFyIHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcclxuXHJcbiAgICAgICAgdmFyIGRhdGE7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcclxuXHJcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKCB0YXJnZXQucmVzcG9uc2VUZXh0KS5jYXN0O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlQWN0ZXVyKGRhdGEpO1xyXG5cclxuXHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG5cclxuXHJcbiAgICBhZmZpY2hlQWN0ZXVyKGRhdGEpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG5cclxuICAgICAgICBmb3IodmFyIGk9MDsgaTx0aGlzLnRvdGFsQWN0ZXVyOyBpKyspe1xyXG5cclxuICAgICAgICAgICAgdmFyIHVuQWN0ZXVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi50ZW1wbGF0ZT4uYWN0ZXVyXCIpLmNsb25lTm9kZSh0cnVlKTtcclxuXHJcblxyXG4gICAgICAgICAgICB1bkFjdGV1ci5xdWVyeVNlbGVjdG9yKFwiaDRcIikuaW5uZXJUZXh0ID0gZGF0YVtpXS5uYW1lO1xyXG5cclxuXHJcbiAgICAgICAgICAgIHVuQWN0ZXVyLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwidzE4NVwiICsgZGF0YVtpXS5wcm9maWxlX3BhdGgpO1xyXG4gICAgICAgICAgICB1bkFjdGV1ci5xdWVyeVNlbGVjdG9yKFwiaW1nXCIpLnNldEF0dHJpYnV0ZShcImFsdFwiLCBkYXRhW2ldLm5hbWUpO1xyXG5cclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5saXN0ZS1hY3RldXJzXCIpLmFwcGVuZENoaWxkKHVuQWN0ZXVyKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgfVxyXG5cclxuXHJcblxyXG59XHJcblxyXG5cclxuXHJcbiJdLCJmaWxlIjoic2NyaXB0LmpzIn0=
