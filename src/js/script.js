document.addEventListener("DOMContentLoaded", function(event) {


    var connexion = new MovieDB();


    if(location.pathname.search("fiche-film.html") > 0){

        var params = ( new URL(document.location) ).searchParams;

        console.log(params.get("id") );

        connexion.requeteInfoFilm(params.get("id") );

    }
    else{
        connexion.requeteFilmPopulaire();
    }

//Un commentaire
    console.log("Ça fonctionne");

    /** Scroll to top button implementation in vanilla JavaScript (ES6 - ECMAScript 6) **/

    var intervalId = 0; // Needed to cancel the scrolling when we're at the top of the page
    var $scrollButton = document.querySelector('.scroll'); // Reference to our scroll button

    function scrollStep() {
        // Check if we're at the top already. If so, stop scrolling by clearing the interval
        if (window.pageYOffset === 0) {
            clearInterval(intervalId);
        }
        window.scroll(0, window.pageYOffset - 50);
    }

    function scrollToTop() {
        // Call the function scrollStep() every 16.66 millisecons
        intervalId = setInterval(scrollStep, 16.66);
    }
    // When the DOM is loaded, this click handler is added to our scroll button
    $scrollButton.addEventListener('click', scrollToTop);






    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
//});

});










class MovieDB{

    constructor(){
        console.log("Parfait 2");

        this.APIKey = "eda01ad95b124c2be1b5f4308d87648f";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;

        this.totalActeur = 3;
    }

    requeteFilmPopulaire(){


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmPopulaire(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse( target.responseText ).results;

            this.afficheFilmPopulaire(data);

            // this.afficheFilmPopulaire2(data);



        }

    }
    // afficheFilmPopulaire2(data) {
    //
    //     console.log(data);
    //
    //     for(var i=0; i<this.totalFilm; i++){
    //
    //
    //         var unArticle = document.querySelector(".template2>.film").cloneNode(true);
    //
    //
    //         unArticle.querySelector("h1").innerText = data[i].title;
    //
    //         unArticle.querySelector("img").setAttribute("src", this.imgPath + "w185" + data[i].poster_path);
    //         unArticle.querySelector("img").setAttribute("alt", data[i].title);
    //
    //
    //
    //
    //         document.querySelector(".swiper-slide").appendChild(unArticle);
    //
    //     }
    //
    // }




    afficheFilmPopulaire(data) {

        console.log(data);

        for(var i=0; i<this.totalFilm; i++){


            var unArticle = document.querySelector(".template>.film").cloneNode(true);


            unArticle.querySelector("h1").innerText = data[i].title;

            if(data[i].overview === ""){
                unArticle.querySelector(".description").innerText = "Description présentement non disponible"
            }
            else{
                unArticle.querySelector(".description").innerText = data[i].overview;
            }

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);
            unArticle.querySelector("img").setAttribute("alt", data[i].title);

            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);


            document.querySelector(".liste-films").appendChild(unArticle);

            document.querySelector(".rating").innerText = data.vote_average;

            document.querySelector(".release").innerText = data.release_date;


        }

    }








    // Requete pour un film en particulier

    requeteInfoFilm(movieId){

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfoFilm.bind(this) );

        // xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();

    }


    retourInfoFilm(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse( target.responseText );

            this.afficheInfoFilm(data);


        }

    }

    afficheInfoFilm(data){

        console.log(data);

        document.querySelector("h1").innerText = data.title;

        if(data.overview === ""){
            document.querySelector(".description2").innerText = "Description présentement non disponible"
        }
        else{
            document.querySelector(".description2").innerText = data.overview;
        }



        document.querySelector("img.affiche").setAttribute("src", this.imgPath + "w500" + data.poster_path);

        this.requeteActeur( data.id );





    }


    requeteActeur(movieId){


        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourActeur.bind(this));

        // xhr.open("GET", "https://api.themoviedb.org/3/credit/%7Bcredit_id%7D?api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId +"/credits?api_key=" + this.APIKey);

        xhr.send();
    }


    retourActeur(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse( target.responseText).cast;

            this.afficheActeur(data);



        }

    }



    afficheActeur(data){
        console.log(data);

        for(var i=0; i<this.totalActeur; i++){

            var unActeur = document.querySelector(".template>.acteur").cloneNode(true);


            unActeur.querySelector("h4").innerText = data[i].name;


            unActeur.querySelector("img").setAttribute("src", this.imgPath + "w185" + data[i].profile_path);
            unActeur.querySelector("img").setAttribute("alt", data[i].name);

            document.querySelector(".liste-acteurs").appendChild(unActeur);

        }



    }



}



